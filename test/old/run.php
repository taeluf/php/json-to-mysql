<?php

require(__DIR__.'/../code/JsonToTable.php');

$source = __DIR__.'/scryfall-oracle-cards.json';
$out = __DIR__.'/scryfall-out/'; // prefix for .json, -gen.json, .sql

$jsonToSql = new JSONToTable($source, $out);

$jsonToSql->generateSchema(false);


$jsonToSql->generateSql('scryfall_import_2020_oct',true, 1024*1024);

echo 'sql generated';
exit; 

$jsonToSql->executeSql();

// 1. Process all rows in json array
    // a. Determine datatypes in all columns
    // b. Max lengths in all columns
// 2. Output schema-gen.json file
// 3. Load in schema.json file || (if not exists) schema-gen.json file
    // - probably separate this step from step 2...
// 4. Loop through JSON data & create insert statements
