# Json To MySQL Test with Scryfall Data

1. Download 'Oracle Cards' JSON file from https://scryfall.com/docs/api/bulk-data
2. Save it as `scryfall-oracle-cards.json` in this folder
3. Create `scryfall-out` directory in this folder
4. Run `php run.php`
5. Verify there are many 1MB files for the INSERT statements
6. Check the first file, a middle file, and the last file for correct-appearing output, including ending with a semi-colon (`;`)

# Verify it is valid SQL
1. ... Uhh, see the root README file for how to import into database. If the statements execute, then it's probably awesome.