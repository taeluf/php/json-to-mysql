<?php

namespace Tlf\Jtm;


class Main extends \Tlf\Tester {

    public function testFullExample(){
        // @export_start(Example.Full)
        // variables you'll want to change
        $source_file = $this->file('test/input/main.json');
        $out_prefix = $this->file('test/input/main.out/test_sql');
        $table_name = 'test_sql';
        // max `strlen()` of the sql put into any one output file
        $max_sql_len = 1024*1024;

        // a pdo instance for running the create & insert
        $pdo = new \PDO("sqlite::memory:");

        // generate all the sql
        $jtm = new \Tlf\Jtm();
        $schema_info = $jtm->generate_schema_info($source_file);
        $schema = $jtm->generate_schema($source_file, $schema_info);
        $create_sql = $jtm->generate_sql_create($table_name, $schema);
        $jtm->generate_sql_insert($source_file, $out_prefix, $table_name, $schema, $max_sql_len);

        //execute the sql
        $pdo->exec($create_sql);
        $num_rows = $jtm->execute_insert_files($out_prefix, $pdo);
        // @export_end(Example.Full)

        $this->compare(
            2,$num_rows
        );

        $stmt = $pdo->query("SELECT * FROM test_sql");
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $this->compare(
            [
                ['name'=>'row1', 'index'=>0],
                ['name'=>'row2', 'index'=>1]
            ],
            $rows
        );

    }

    public function testSqlGenInsert(){
        $jtm = new \Tlf\Jtm();
        $jtm->disable_foreign_key_checks = true;
        $source_file = $this->file('test/input/main.json');
        $schema_info = $jtm->generate_schema_info($source_file);
        $schema = $jtm->generate_schema($source_file, $schema_info);
        $sql = $jtm->generate_sql_insert($source_file, $this->file('test/input/main.out/test_sql'), 'test_sql', $schema, 9999);

        echo 
        $this->compare_lines(
            <<<SQL
                SET FOREIGN_KEY_CHECKS=0;
                INSERT INTO `test_sql` 
                    ( `name`,`index` )
                VALUES
                    ( 'row1', 0),
                    ( 'row2', 1)
                ;
                SET FOREIGN_KEY_CHECKS=0;
            SQL,
            file_get_contents($this->file('test/input/main.out/test_sql-insert-0.sql'))
        );
    }

    public function testSqlGenCreate(){
        $jtm = new \Tlf\Jtm();
        $schema_info = $jtm->generate_schema_info($this->file('test/input/main.json'));
        $schema = $jtm->generate_schema($this->file('test/input/main.json'), $schema_info);
        $sql = $jtm->generate_sql_create('test_sql', $schema);
        // echo $sql;
        $target = <<<SQL
            DROP TABLE IF EXISTS `test_sql`;

            CREATE TABLE `test_sql`
            (
            `name` VARCHAR(54), `index` int(10)
            )
            ;
        SQL;
        $this->compare_lines($target, $sql);
    }

    public function testSchemaGeneration(){
        $jtm = new \Tlf\Jtm();
        $schema_info = $jtm->generate_schema_info($this->file('test/input/main.json'));
        $schema = $jtm->generate_schema($this->file('test/input/main.json'), $schema_info);
        // print_r($schema);
        $this->compare(
            [
                'name'=>[
                    'name'=>'name',
                    'type'=>'VARCHAR(54)'
                ],
                'index'=>[
                    'name'=>'index',
                    'type'=>'int(10)'
                ]
            ],
            $schema
        );
    }
    public function testSchemaInfo(){
        $jtm = new \Tlf\Jtm();
        
        $schema_info = $jtm->generate_schema_info($this->file('test/input/main.json'));
        // print_r($schema_info);

        $this->compare(
            // @export_start(Example.ColumnData)
            [
            'name' => [
                
                    'maxlen' => 4,
                    'minlen' => 4,
                    'count' => 2,
                    'is_string' => 2,
                    'is_numeric' => 0,
                    'is_int' => 0,
                    'is_float' => 0,
                    'is_bool' => 0,
                    'is_array' => 0,
                    'is_date' => 0,
                    'is_json' => 0,
                    'sampleValue' => 'row2',
                ],
            'index'=>[
                    'maxlen' => 1,
                    'minlen' => 1,
                    'count' => 2,
                    'is_string' => 0,
                    'is_numeric' => 2,
                    'is_int' => 2,
                    'is_float' => 2,
                    'is_bool' => 0,
                    'is_array' => 0,
                    'is_date' => 0,
                    'is_json' => 0,
                    'sampleValue' => 1,
                ]
            ],
            // @export_end(Example.ColumnData)
            $schema_info
        );
    }

    public function testRowToArray(){
        $jtm = new \Tlf\Jtm();
        $row = $jtm->json_line_to_array('{"name":"row", "index":0},');
        $this->compare(
            $row,
            ['name'=>'row',
            'index'=>0]
        );

        $row = $jtm->json_line_to_array('{"name":"row2", "index":1}');
        $this->compare(
            $row,
            [
            'name'=>'row2',
            'index'=>1,
            ]
        );
    }
}
