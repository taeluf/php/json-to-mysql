# File code/Jtm.php  
  
# class Tlf\Jtm  
Json To Mysql class  
See source code at [/code/Jtm.php](/code/Jtm.php)  
  
## Constants  
  
## Properties  
- `public bool $disable_foreign_key_checks = false;` Set `true` to disable foreign key checks during INSERTs  
  
## Methods   
- `public function json_line_to_array(string $line): array` Convert a json line into an array  
  
- `public function is_date($value): bool` check if a value is a date  
- `public function clean_schema(array $dirty_schema): array` convert column names to camel_case  
  
- `public function execute_insert_files(string $prefix, \PDO $pdo): int`   
- `public function generate_schema(string $source_file, array $schema_info): array`   
- `public function generate_schema_info($source_file)`   
- `public function generate_sql_create(string $tableName, array $schema, bool $dropIfExists=true)`   
- `public function generate_sql_insert(string $source_file, string $out_prefix, string $tableName, array $schema, $chunkByLength, callable $rowModifier = null)` Generate sql for INSERTs and write it to disk   
  
  
